# TestICAP

This project is CRUD, we can Sign in and Log in.
We can access to the tab of all of the kids, we can add new kid information, update the informations and delete the information of a kid.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

You can find the dashboard to `http://localhost:4200/dashboard`
You can find the Log In page to `http://localhost:4200/login`
You can find the Sign in to `http://localhost:4200/signin`


## Json Server

I have use the json serve for my data

Run `json-server --watch db.json` for open the server to `http://localhost:3000/`

You can find the data of the log in to `http://localhost:3000/signupUsers`