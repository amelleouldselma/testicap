import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { KidsDashboardComponent } from './kids-dashboard/kids-dashboard.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  {path:'login', component: LoginComponent},
  {path: 'dashboard', component: KidsDashboardComponent},
  {path: 'signup', component: SignupComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
