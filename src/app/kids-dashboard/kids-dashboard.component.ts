import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms'
import { ApiService } from '../shared/api.service';
import { KidsModel } from './kids-dash board.model';


@Component({
  selector: 'app-kids-dashboard',
  templateUrl: './kids-dashboard.component.html',
  styleUrls: ['./kids-dashboard.component.css']
})
export class KidsDashboardComponent implements OnInit {

  formValue !: FormGroup;
  kidsModelObj : KidsModel = new KidsModel();
  kidsData !: any;
  constructor(private formbuilder: FormBuilder,
  private api : ApiService) { }


  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      firstName : [''],
      lastName : [''],
      birth : [''],
    })
    this.getAllKids();
  }
  postKidsDetails(){
    this.kidsModelObj.firstName = this.formValue.value.firstName;
    this.kidsModelObj.firstName = this.formValue.value.firstName;
    this.kidsModelObj.lastName = this.formValue.value.lastName;
    this.kidsModelObj.birth = this.formValue.value.birth;

    this.api.postKids(this.kidsModelObj)
    .subscribe(res=>{
      console.log(res);
      alert("Kid Added Successfully")
      let ref = document.getElementById('cancel') 
      ref?.click();
      this.formValue.reset();
      this.getAllKids();
    },
    err=>{
      alert("Something went wrong ")
    })
  }
  getAllKids(){
    this.api.getKids()
    .subscribe(res=>{ 
      this.kidsData = res;
    })
  }
  deleteKids(row : any){
    this.api.deleteKids(row.id)
    .subscribe(res=>{
      alert("Kid information Deleted");
     this.getAllKids();
    })
  }
  onEdit(row : any){
    this.kidsModelObj.id = row.id;
    this.formValue.controls['firstName'].setValue(row.firstname);
    this.formValue.controls['lastName'].setValue(row.lastName);
    this.formValue.controls['birth'].setValue(row.birth);
  }
  updateKidsDetails(){
    this.kidsModelObj.firstName = this.formValue.value.firstName;
    this.kidsModelObj.firstName = this.formValue.value.firstName;
    this.kidsModelObj.lastName = this.formValue.value.lastName;
    this.kidsModelObj.birth = this.formValue.value.birth;

    this.api.updateKids(this.kidsModelObj,this.kidsModelObj.id)
    .subscribe(res=>{
      alert("Updated Scuccesfuly");
      let ref = document.getElementById('cancel')
      ref?.click(); 
      this.formValue.reset();
      this.getAllKids(); 
    })
  }

}
